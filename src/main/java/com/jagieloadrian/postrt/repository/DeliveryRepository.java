package com.jagieloadrian.postrt.repository;

import com.jagieloadrian.postrt.model.Delivery;
import com.jagieloadrian.postrt.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeliveryRepository extends JpaRepository<Delivery, Long> {

    Delivery getDeliveryByUniqueNumberDelivery(String numberDelivery);

    Delivery getDeliveryByUserName(String userName);

    boolean existsByUserName(String userName);

    List<Delivery> findAllByStatus(Status status);

    int countDeliveryByStatus(Status status);

}
