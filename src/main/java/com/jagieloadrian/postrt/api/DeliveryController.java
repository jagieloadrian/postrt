package com.jagieloadrian.postrt.api;

import com.jagieloadrian.postrt.model.Delivery;
import com.jagieloadrian.postrt.model.request.CreateDeliveryCommand;
import com.jagieloadrian.postrt.model.request.ReaderDeliveryModel;
import com.jagieloadrian.postrt.service.DeliveryService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/post")
class DeliveryController {

    private final DeliveryService deliveryService;

    DeliveryController(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Delivery addDelivery(@RequestBody @Valid CreateDeliveryCommand delivery) {
        return deliveryService.addDelivery(delivery);
    }

    @PostMapping("/postDeliveries")
    @ResponseStatus(HttpStatus.CREATED)
    void addTemplateDeliveries(){
        deliveryService.addTemplateDeliveries();
    }

    @DeleteMapping("/deleteDeliveries")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteAllDeliveries(){
        deliveryService.deleteAllDeliveries();
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    String clientBeforeInQueueAndEstimatedTime(String uniqueDeliveryNumberOrUserName) {
        return deliveryService
                .clientBeforeInQueueAndEstimatedTime(uniqueDeliveryNumberOrUserName);
    }

    @GetMapping("/list")
    @ResponseStatus(HttpStatus.OK)
    List<ReaderDeliveryModel> getAllDeliveries() {
        return deliveryService.getAllDeliveries();
    }

}
