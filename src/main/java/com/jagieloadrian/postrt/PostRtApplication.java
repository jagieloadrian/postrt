package com.jagieloadrian.postrt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostRtApplication {

    public static void main(String[] args) {
        SpringApplication.run(PostRtApplication.class, args);
    }

}
