package com.jagieloadrian.postrt.service;

import com.jagieloadrian.postrt.exception.DeliveryError;
import com.jagieloadrian.postrt.exception.DeliveryException;
import com.jagieloadrian.postrt.model.Delivery;
import com.jagieloadrian.postrt.model.Status;
import com.jagieloadrian.postrt.model.request.CreateDeliveryCommand;
import com.jagieloadrian.postrt.model.request.ReaderDeliveryModel;
import com.jagieloadrian.postrt.repository.DeliveryRepository;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
class DeliveryServiceImpl implements DeliveryService {

    private final DeliveryRepository repository;

    DeliveryServiceImpl(DeliveryRepository repository) {
        this.repository = repository;
    }

    public Delivery addDelivery(CreateDeliveryCommand deliveryCommand) {
        if (deliveryCommand.getStatus().equals(Status.VIP)) {
            isCorrectPINForVIP(deliveryCommand.getPin());
        }
        if (deliveryCommand.getStatus().equals(Status.URGENT)) {
            isCorrectPINForUrgent(deliveryCommand.getPin());
        }
        if (repository.existsByUserName(deliveryCommand.getUserName())) {
            throw new DeliveryException(DeliveryError.USERNAME_IS_NOT_FREE);
        }
        Delivery delivery = Delivery.builder()
                .userName(deliveryCommand.getUserName())
                .status(deliveryCommand.getStatus())
                .uniqueNumberDelivery(generateUniqueNumberDelivery())
                .build();
        repository.save(delivery);
        return delivery;
    }

    @Override
    public List<ReaderDeliveryModel> getAllDeliveries() {
        List<ReaderDeliveryModel> modelList = new LinkedList<>();
        List<Delivery> listUrgent = getDeliveriesUrgent();
        addDeliveryReaderToList(modelList, listUrgent);
        List<Delivery> listVIP = getDeliveriesVIP();
        addDeliveryReaderToList(modelList, listVIP);
        List<Delivery> listNormal = getDeliveriesNormal();
        addDeliveryReaderToList(modelList, listNormal);
        return modelList;
    }

    public String clientBeforeInQueueAndEstimatedTime(String uniqueDeliveryNumberOrUserName) {
        int numberInQueue;
        int estimatedTime;
        Delivery delivery;
        if (uniqueDeliveryNumberOrUserName.matches("[0-9]+")) {
            delivery = getDeliveryByUniqueNumber(uniqueDeliveryNumberOrUserName);
            numberInQueue = clientsBeforeInQueue(uniqueDeliveryNumberOrUserName);
        } else {
            delivery = repository
                    .getDeliveryByUserName(uniqueDeliveryNumberOrUserName);
            numberInQueue = clientsBeforeInQueue((delivery.getUniqueNumberDelivery()));
        }
        estimatedTime = estimatedTime(delivery);
        return String.format("Liczba osób oczekujących przed Tobą: %d\nTwój czas oczekiwania to: %s sekund",
                numberInQueue, estimatedTime);
    }

    @Override
    public void deleteAllDeliveries() {
        repository.deleteAll();
    }

    @Override
    public void addTemplateDeliveries() {
        DeliveryTemplateService deliveryTemplateService = new DeliveryTemplateService();
        List<CreateDeliveryCommand> addList = deliveryTemplateService.modelList();
        for (CreateDeliveryCommand command : addList) {
            addDelivery(command);
        }
    }

    private void addDeliveryReaderToList(List<ReaderDeliveryModel> modelList, List<Delivery> deliveryList) {
        for (Delivery delivery : deliveryList) {
            modelList.add(readerDeliveryModelMapper(delivery));
        }
    }

    private int clientsBeforeInQueue(String uniqueNumberDelivery) {
        Delivery delivery = getDeliveryByUniqueNumber(uniqueNumberDelivery);
        List<Delivery> modelList = new LinkedList<>();
        int temp = 0;
        if (delivery.getStatus().equals(Status.URGENT)) {
            modelList.addAll(getDeliveriesUrgent());
            temp = modelList.indexOf(delivery);
        }
        if (delivery.getStatus().equals(Status.VIP)) {
            modelList.addAll(getDeliveriesUrgent());
            modelList.addAll(getDeliveriesVIP());
            temp = modelList.indexOf(delivery);
        }
        if (delivery.getStatus().equals(Status.NORMAL)) {
            modelList.addAll(getDeliveriesUrgent());
            modelList.addAll(getDeliveriesVIP());
            modelList.addAll(getDeliveriesNormal());
            modelList.indexOf(delivery);
            temp = modelList.indexOf(delivery);
        }
        return temp;
    }

    private int estimatedTime(Delivery delivery) {
        int temp = 0;
        if (delivery.getStatus().equals(Status.VIP)) {
            temp = estimatedTimeForVIP((delivery.getUniqueNumberDelivery()));
        }
        if (delivery.getStatus().equals(Status.NORMAL)) {
            temp = estimatedTimeForNormal((delivery.getUniqueNumberDelivery()));
        }
        return temp;
    }

    private int estimatedTimeForNormal(String uniqueNumberDelivery) {
        int tempNumbVIP = repository.countDeliveryByStatus(Status.VIP) * 20;
        int tempNumbUrgent = repository.countDeliveryByStatus(Status.URGENT) * 60;
        List<Delivery> tempList = getDeliveriesNormal();
        int temp = tempList.indexOf(getDeliveryByUniqueNumber(uniqueNumberDelivery));
        int tempNumbNormal = temp * 20;
        return tempNumbNormal + tempNumbUrgent + tempNumbVIP;
    }

    private int estimatedTimeForVIP(String uniqueNumberDelivery) {
        int tempNumbUrgent = repository.countDeliveryByStatus(Status.URGENT) * 60;
        List<Delivery> tempList = getDeliveriesVIP();
        int temp = tempList.indexOf(getDeliveryByUniqueNumber(uniqueNumberDelivery));
        int tempNumbVip = temp * 20;
        return tempNumbVip + tempNumbUrgent;
    }

    private Delivery getDeliveryByUniqueNumber(String uniqueNumber) {
        return repository.getDeliveryByUniqueNumberDelivery(uniqueNumber);
    }

    String generateUniqueNumberDelivery() {
        return String.valueOf((long) Math.floor(Math.random() * 900000000L) + 10000000L);
    }

    private List<Delivery> getDeliveriesUrgent() {
        return getDeliveries(Status.URGENT);
    }

    private List<Delivery> getDeliveriesVIP() {
        return getDeliveries(Status.VIP);
    }

    private List<Delivery> getDeliveriesNormal() {
        return getDeliveries(Status.NORMAL);
    }

    private List<Delivery> getDeliveries(Status status) {
        return repository.findAllByStatus(status);
    }

    void isCorrectPINForVIP(int pin) {
        if (pin != 8888) {
            throw new DeliveryException(DeliveryError.INCORRECT_PIN);
        }
    }

    void isCorrectPINForUrgent(int pin) {
        if (pin != 0000) {
            throw new DeliveryException(DeliveryError.INCORRECT_PIN);
        }
    }

    ReaderDeliveryModel readerDeliveryModelMapper(Delivery delivery) {
        return ReaderDeliveryModel
                .builder()
                .uniqueNumberDelivery(delivery.getUniqueNumberDelivery())
                .userName(delivery.getUserName()).build();
    }
}