package com.jagieloadrian.postrt.service;

import com.jagieloadrian.postrt.model.Delivery;
import com.jagieloadrian.postrt.model.request.CreateDeliveryCommand;
import com.jagieloadrian.postrt.model.request.ReaderDeliveryModel;

import java.util.List;

public interface DeliveryService {

    Delivery addDelivery(CreateDeliveryCommand deliveryCommand);

    List<ReaderDeliveryModel> getAllDeliveries();

    String clientBeforeInQueueAndEstimatedTime(String uniqueDeliveryNumberOrUserName);

    void deleteAllDeliveries();

    void addTemplateDeliveries();
}
