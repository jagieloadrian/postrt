package com.jagieloadrian.postrt.service;

import com.jagieloadrian.postrt.model.Status;
import com.jagieloadrian.postrt.model.request.CreateDeliveryCommand;

import java.util.LinkedList;
import java.util.List;

class DeliveryTemplateService {

    private static CreateDeliveryCommand firstDelivery = new CreateDeliveryCommand();

    private static void setFirstDelivery() {
        firstDelivery.setUserName("Adam");
        firstDelivery.setStatus(Status.NORMAL);
        firstDelivery.setPin(0);
    }

    private static CreateDeliveryCommand secondDelivery = new CreateDeliveryCommand();

    private static void setSecondDelivery() {
        secondDelivery.setUserName("Kamil");
        secondDelivery.setStatus(Status.VIP);
        secondDelivery.setPin(8888);
    }

    private static CreateDeliveryCommand thirdDelivery = new CreateDeliveryCommand();

    private static void setThirdDelivery() {
        thirdDelivery.setUserName("Michał");
        thirdDelivery.setStatus(Status.URGENT);
        thirdDelivery.setPin(0000);
    }

    private static CreateDeliveryCommand fourthDelivery = new CreateDeliveryCommand();

    private static void setFourthDelivery() {
        fourthDelivery.setUserName("Adrianna");
        fourthDelivery.setStatus(Status.VIP);
        fourthDelivery.setPin(8888);
    }

    private static CreateDeliveryCommand fifthDelivery = new CreateDeliveryCommand();

    private static void setFifthDelivery() {
        fifthDelivery.setUserName("Leokadia");
        fifthDelivery.setStatus(Status.NORMAL);
        fifthDelivery.setPin(0);
    }

    private static void setDeliveries() {
        setFirstDelivery();
        setSecondDelivery();
        setThirdDelivery();
        setFourthDelivery();
        setFifthDelivery();
    }

    List<CreateDeliveryCommand> modelList() {
        List<CreateDeliveryCommand> list = new LinkedList<>();
        setDeliveries();
        list.add(firstDelivery);
        list.add(secondDelivery);
        list.add(thirdDelivery);
        list.add(fourthDelivery);
        list.add(fifthDelivery);
        return list;
    }

}
