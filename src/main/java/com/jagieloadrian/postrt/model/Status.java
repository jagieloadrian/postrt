package com.jagieloadrian.postrt.model;

public enum Status {
    VIP, NORMAL, URGENT
}
