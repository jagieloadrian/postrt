package com.jagieloadrian.postrt.model.request;

import com.jagieloadrian.postrt.model.Status;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CreateDeliveryCommand {
    @NotBlank
    @Size(min = 3)
    private String userName;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    private int pin;
}
