package com.jagieloadrian.postrt.model.request;


import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Builder
public class ReaderDeliveryModel {

    @NotBlank
    private String userName;

    private String uniqueNumberDelivery;
}
