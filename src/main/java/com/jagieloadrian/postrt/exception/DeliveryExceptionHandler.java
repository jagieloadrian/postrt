package com.jagieloadrian.postrt.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class DeliveryExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ErrorInfo> handleExcpetion(DeliveryException e) {
        return ResponseEntity.status(e.getDeliveryError().getStatus())
                .body(new ErrorInfo(e.getDeliveryError().getMessage()));
    }

}
