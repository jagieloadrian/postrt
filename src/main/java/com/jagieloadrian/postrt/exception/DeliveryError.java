package com.jagieloadrian.postrt.exception;

import org.springframework.http.HttpStatus;

public enum DeliveryError {
    INCORRECT_PIN("The PIN is incorrect",HttpStatus.BAD_REQUEST),
    USERNAME_IS_NOT_FREE("This username is not free", HttpStatus.BAD_REQUEST);

    private String message;
    private HttpStatus status;

    DeliveryError(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    HttpStatus getStatus() {
        return status;
    }
}
