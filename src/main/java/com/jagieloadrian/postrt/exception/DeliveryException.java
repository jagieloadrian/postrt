package com.jagieloadrian.postrt.exception;

public class DeliveryException extends RuntimeException {
    private final DeliveryError deliveryError;

    public DeliveryException(DeliveryError deliveryError) {
        this.deliveryError = deliveryError;
    }

    public DeliveryError getDeliveryError() {
        return deliveryError;
    }
}
