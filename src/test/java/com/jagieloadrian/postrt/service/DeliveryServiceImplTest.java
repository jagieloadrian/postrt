package com.jagieloadrian.postrt.service;

import com.jagieloadrian.postrt.exception.DeliveryError;
import com.jagieloadrian.postrt.exception.DeliveryException;
import com.jagieloadrian.postrt.model.Delivery;
import com.jagieloadrian.postrt.model.Status;
import com.jagieloadrian.postrt.model.request.ReaderDeliveryModel;
import com.jagieloadrian.postrt.repository.DeliveryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DeliveryServiceImplTest {
    DeliveryRepository repository = Mockito.mock(DeliveryRepository.class);

    DeliveryServiceImpl service = new DeliveryServiceImpl(repository);

    DeliveryServiceImpl deliveryService = Mockito.mock(DeliveryServiceImpl.class);

    @Test
    @DisplayName("should thrown exception when pin for VIP is incorrect")
    void should_Thrown_Exception_Pin_Is_Incorrect_VIP() {
        //given
        int pin = 1234;
        //when
        deliveryService.isCorrectPINForVIP(pin);
        //then
        assertThatExceptionOfType(DeliveryException.class).
                isThrownBy(() -> {
                    throw new DeliveryException(DeliveryError.INCORRECT_PIN);
                });
    }

    @Test
    @DisplayName("should thrown exception when pin for URGENT is incorrect")
    void should_Thrown_Exception_Pin_Is_Incorrect_URGENT() {
        //given
        int pin = 1234;
        //when
        deliveryService.isCorrectPINForVIP(pin);
        //then
        assertThatExceptionOfType(DeliveryException.class).
                isThrownBy(() -> {
                    throw new DeliveryException(DeliveryError.INCORRECT_PIN);
                });

    }

    @Test
    @DisplayName("should generate unique number delivery correct")
    void should_Generate_UniqueNumber_Correctly() {
        //given
        String str = service.generateUniqueNumberDelivery();
        //then
        Assertions.assertEquals(9, str.length());
    }


    @Test
    void should_createDeliveryReaderModel_Correctly() {
        //given
        Delivery temp = Delivery.builder()
                .uniqueNumberDelivery("12345")
                .userName("ADAM")
                .build();
        //when
        ReaderDeliveryModel model = service.readerDeliveryModelMapper(temp);
        //then
        assertEquals("12345", model.getUniqueNumberDelivery());
        assertEquals("ADAM", model.getUserName());
    }
}